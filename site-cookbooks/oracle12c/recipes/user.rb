#
# Cookbook Name:: oracle12c
# Recipe:: user
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

####################
## Create OS groups
groups = { 
  "oinstall" => "54321",
  "dba" => "54322",
  "backupdba" => "54323",
  "oper" => "54324",
  "dgdba" => "54325",
  "kmdba" => "54326"
}

groups.each do |name, gid|
  group name do
    gid gid
  end
end

####################
## Create OS user
user "oracle" do
  uid 1200
  gid "oinstall"
  supports :manage_home => true
  home "/home/oracle"
  password "$1$YnHMPdkH$/KqjmWACS3iTb/.sfxMg30"
end

groups.each_key do |name|
  group name do
    members "oracle"
    action :modify
  end
end
