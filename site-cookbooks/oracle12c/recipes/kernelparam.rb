#
# Cookbook Name:: oracle12c
# Recipe:: kernelparam
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

####################
# Configure kernel parameter

cookbook_file "/etc/sysctl.conf" do
  mode 00644
  notifies :run, "execute[sysctl-p]", :immediately
end

execute "sysctl-p" do
  command "/sbin/sysctl -p"
  action :nothing
end
