#
# Cookbook Name:: oracle12c
# Recipe:: envvalue
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

###################
# Add environment variables

cookbook_file "/home/oracle/.bash_profile" do
  mode 00644
  owner "oracle"
  group "oinstall"
end

