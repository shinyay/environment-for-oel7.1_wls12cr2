#
# Cookbook Name:: oracle12c
# Recipe:: directory
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

dirs = [
  "/u01",
  "/u01/app",
  "/u01/app/oracle",
  "/u01/app/oracle/product",
  "/u01/app/oracle/product/12.2.1",
  "/u01/app/oracle/config",
  "/u01/app/oracle/config/domains",
  "/u01/app/oracle/config/applications",
  "/u01/app/oraInventory"
]

dirs.each do |dir|
  directory dir do
    mode 00755
    owner "oracle"
    group "oinstall"
  end
end
